public class Customer implements Comparable<Customer> {
    private String shippingMode;
    private String name;
    private String segment;
    private String category;
    private String subCategory;
    private double sales;
    private double discount;
    private double profit;

    public Customer(String shippingMode, String name, String segment, String category,
                    String subCategory, double sales, double discount, double profit) {
        this.shippingMode = shippingMode;
        this.name = name;
        this.segment = segment;
        this.category = category;
        this.subCategory = subCategory;
        this.sales = sales;
        this.discount = discount;
        this.profit = profit;

    }

    @Override
    public int compareTo(Customer o) {
        return 0;
    }

    public String getShippingMode() {
        return shippingMode;
    }

    public String getName() {
        return name;
    }

    public String getSegment() {
        return segment;
    }

    public String getCategory() {
        return category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public double getSales() {
        return sales;
    }

    public double getDiscount() {
        return discount;
    }

    public double getProfit() {
        return profit;
    }

    public void setShippingMode(String shippingMode) {
        this.shippingMode = shippingMode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public void setSales(double sales) {
        this.sales = sales;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
}
