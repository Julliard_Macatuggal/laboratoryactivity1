import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Customer> customers = readFile("res/SuperstoreDatasetCopy.csv");
        showPurchaseCategory(customers);
        //showPurchaseSubCategory(customers);
        //showPurchasePerPerson(customers);
        //shippingMode(customers);
        //purchasedFor(customers);

    }

    public static List<Customer> readFile(String fileName) {
        try {
            List<Customer> customers = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(fileName));

            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                String[] data = line.split(",");

                Customer customer = new Customer(data[0], data[1], data[2], data[3], data[4],
                        Double.parseDouble(data[5]), Double.parseDouble(data[6]), Double.parseDouble(data[7]));
                customers.add(customer);

            }
            br.close();
            return customers;
        } catch (Exception e) {
            throw new RuntimeException("Data file read error.");
        }
    }

    public static void showPurchaseCategory(List<Customer> customers) {
        System.out.println("Purchases per category:");
        Map<String, List<Customer>> category = customers.stream().sorted(Comparator.comparing(Customer::getCategory))
                .collect(Collectors.groupingBy(Customer::getCategory));
        Set<String> customer = category.keySet();
        for (String categories : customer) {
            List<Customer> cat = category.get(categories);
            int total = (int) cat.stream().count();
            System.out.printf("Category of %s has a total purchase of %d%n", categories, +total);

        }
        System.out.println();
    }

    public static void showPurchaseSubCategory(List<Customer> customers) {
        System.out.println("Purchases per Sub-category:");
        Map<String, List<Customer>> subCategory = customers.stream()
                .collect(Collectors.groupingBy(Customer::getSubCategory));
        Set<String> customer = subCategory.keySet();
        for (String subCategories : customer) {
            List<Customer> subCat = subCategory.get(subCategories);
            int total = (int) subCat.stream().count();
            System.out.printf("Sub-category of %s has a total purchase of %d%n", subCategories, +total);

        }
        System.out.println();
    }

    public static void showPurchasePerPerson(List<Customer> customers) {
        //ArrayList<Customer> nameOccurence = new ArrayList<>();

        //System.out.println("Ascending order per purchase of customer:");
        System.out.println("Purchases per Person:");
        Map<String, List<Customer>> name = customers.stream()
                .collect(Collectors.groupingBy(Customer::getName));
        Set<String> customer = name.keySet();
        for(String person : customer) {
            List<Customer> customers1 = name.get(person);
            int total = (int) customers1.stream().count();
            System.out.printf("%s has a total purchase of %d%n" , person, + total);

        }
        System.out.println();
    }

    public static void purchasedFor(List<Customer> customers) {
        System.out.println("Purchased items are used for:");
        Map<String, List<Customer>> segmentUsed = customers.stream()
                .collect(Collectors.groupingBy(Customer::getSegment));
        Set<String> customer = segmentUsed.keySet();
        for (String segments : customer) {
            List<Customer> segment = segmentUsed.get(segments);
            int total = (int) segment.stream().count();
            System.out.printf("%d items has been used for %s \n", total, segments);

        }
        System.out.println();
    }

    public static void shippingMode(List<Customer> customers) {
        System.out.println("Shipping mode used:");
        Map<String, List<Customer>> shippingMode = customers.stream()
                .collect(Collectors.groupingBy(Customer::getShippingMode));
        Set<String> customer = shippingMode.keySet();
        for (String type : customer) {
            List<Customer> numberOfThatShippingMode = shippingMode.get(type);
            int total = (int) numberOfThatShippingMode.stream().count();
            System.out.printf("%s shipping has been used %d%n", type, + total);

        }
        System.out.println();
    }


}
